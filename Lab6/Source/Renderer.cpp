#include "Renderer.h"
#include "GeometryNode.h"
#include "Tools.h"
#include "ShaderProgram.h"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "OBJLoader.h"

#include <algorithm>
#include <array>
#include <iostream>

// RENDERER
Renderer::Renderer()
{
	this->m_nodes = {};
	this->m_continous_time = 0.0;
}

Renderer::~Renderer()
{
	glDeleteTextures(1, &m_fbo_depth_texture);
	glDeleteTextures(1, &m_fbo_pos_texture);
	glDeleteTextures(1, &m_fbo_normal_texture);
	glDeleteTextures(1, &m_fbo_albedo_texture);
	glDeleteTextures(1, &m_fbo_mask_texture);

	glDeleteFramebuffers(1, &m_fbo);

	glDeleteVertexArrays(1, &m_vao_fbo);
	glDeleteBuffers(1, &m_vbo_fbo_vertices);
}

bool Renderer::Init(int SCREEN_WIDTH, int SCREEN_HEIGHT)
{
	this->m_screen_width = SCREEN_WIDTH;
	this->m_screen_height = SCREEN_HEIGHT;

	bool techniques_initialization = InitShaders();

	bool meshes_initialization = InitGeometricMeshes();
	bool light_initialization = InitLights();

	bool common_initialization = InitCommonItems();
	bool inter_buffers_initialization = InitIntermediateBuffers();

	//If there was any errors
	if (Tools::CheckGLError() != GL_NO_ERROR)
	{
		printf("Exiting with error at Renderer::Init\n");
		return false;
	}

	this->BuildWorld();
	this->InitCamera();

	//If everything initialized
	return techniques_initialization && meshes_initialization &&
		common_initialization && inter_buffers_initialization;
}

void Renderer::BuildWorld()
{

	const float CORRIDOR_LEN = 20.f;

	GeometryNode& beam = *this->m_nodes[0];

	std::vector<std::reference_wrapper <GeometryNode>> pipe;
	for (int i = 1; i < 13; i++) {
		pipe.push_back(*this->m_nodes[i]);
	}

	std::vector<std::reference_wrapper <GeometryNode>> iris;
	for (int i = 13; i < 16; i++) {
		iris.push_back(*this->m_nodes[i]);
	}

	std::vector<std::reference_wrapper <GeometryNode>> cannonMount;
	for (int i = 16; i < 19; i++) {
		cannonMount.push_back(*this->m_nodes[i]);
	}

	std::vector<std::reference_wrapper <GeometryNode>> cannon;
	for (int i = 19; i < 22; i++) {
		cannon.push_back(*this->m_nodes[i]);
	}

	std::vector<std::reference_wrapper <GeometryNode>> wall;
	for (int i = 22; i < 31; i++) {
		wall.push_back(*this->m_nodes[i]);
	}

	std::vector<std::reference_wrapper <GeometryNode>> corridorFork;
	for (int i = 31; i < 34; i++) {
		corridorFork.push_back(*this->m_nodes[i]);
	}

	std::vector<std::reference_wrapper <GeometryNode>> corridorRight;
	for (int i = 34; i < 42; i++) {
		corridorRight.push_back(*this->m_nodes[i]);
	}

	std::vector<std::reference_wrapper <GeometryNode>> corridorLeft;
	for (int i = 42; i < 50; i++) {
		corridorLeft.push_back(*this->m_nodes[i]);
	}

	std::vector<std::reference_wrapper <GeometryNode>> corridorStraight;
	for (int i = 50; i < 60; i++) {
		corridorStraight.push_back(*this->m_nodes[i]);
	}

	
	corridorStraight[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -20.f));
	corridorStraight[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -40.f));

	pipe[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -20.f));
	pipe[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -30.f));
	pipe[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -40.f));
	pipe[3].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -50.f));
	pipe[4].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.2, 4.f, -57.5)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));


	//split
	corridorFork[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -60.f));

	//left path
	float left_z_pos = -60.f;
	corridorLeft[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 0.f, left_z_pos += -CORRIDOR_LEN ));
	corridorLeft[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-10.f, 0.f, left_z_pos += -CORRIDOR_LEN));

	wall[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-7.f, 0.f, left_z_pos+1.f));

	this->wall_pos.push_back(glm::vec3(-7.f, 0.f, left_z_pos + 1.f));
	this->wall_update_translate.push_back(0.f);

	corridorLeft[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-15.f, 0.f, left_z_pos += -CORRIDOR_LEN));

	beam.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-16.f, -3.f, left_z_pos-1.f));

	corridorRight[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-20.f, 0.f, left_z_pos += -CORRIDOR_LEN));
	corridorRight[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-15.f, 0.f, left_z_pos += -CORRIDOR_LEN));

	wall[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-15.f, -3.f, left_z_pos-2.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-21.f), glm::vec3(0.f, 1.f, 0.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));
	wall_pos.push_back(glm::vec3(-15.f, -3.f, left_z_pos - 2.f));
	wall_update_translate.push_back(0.f);

	cannonMount[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-13.f, -2.5, left_z_pos - 6.f)) * glm::translate(glm::mat4(1.f), glm::vec3(0.8, -0.2, 5.f)) * glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(0.f, 1.f, 0.f)) * glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(1.f, 0.f, 0.f));
	cannon[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-13.f, -2.5, left_z_pos - 6.f)) * glm::translate(glm::mat4(1.f), glm::vec3(0.f, 1.15, 5.f));

	cannon_pos.push_back(glm::vec3(-13.f, -2.5 +1.5, left_z_pos - 6.f +5));
	cannon_update_radians.push_back(glm::vec3(0.f, 0.f, 0.f));

	corridorRight[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-10.f, 0.f, left_z_pos += -CORRIDOR_LEN));

	wall[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-14.f, 0.f, left_z_pos - 0.f));
		
	this->wall_pos.push_back(glm::vec3(-14.f, 0.f, left_z_pos - 0.f));
	this->wall_update_translate.push_back(0.f);

	cannonMount[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-14.f, -3.2, left_z_pos - 0.f)) * glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, 0.f)) * glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(1.f, 0.f, 0.f));
	cannon[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-14.f, -3.f, left_z_pos - 0.f)) * glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.5, 1.f)) * glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));

	cannon_pos.push_back(glm::vec3(-14.f, -3 + 0.5, left_z_pos - 0.f + 1));
	cannon_update_radians.push_back(glm::vec3(0.f, 0.f, 0.f));

	iris[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-14.f, 3.f, left_z_pos - 0.f));
	iris_pos.push_back(glm::vec3(-14.f, 3.f, left_z_pos - 0.f));
	iris_update_translate.push_back(0.f);


	//right path
	float right_z_pos = -60.f;
	corridorRight[3].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(5.f, 0.f, right_z_pos += -CORRIDOR_LEN));

	wall[3].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(8.f, 0.f, right_z_pos - 1.f));

	this->wall_pos.push_back(glm::vec3(8.f, 0.f, right_z_pos - 1.f));
	this->wall_update_translate.push_back(0.f);;

	corridorRight[4].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(10.f, 0.f, right_z_pos += -CORRIDOR_LEN));

	wall[4].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(7.f, 0.f, right_z_pos - 1.5));

	this->wall_pos.push_back(glm::vec3(7.f, 0.f, right_z_pos - 1.5));
	this->wall_update_translate.push_back(0.f);;

	corridorRight[5].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(15.f, 0.f, right_z_pos += -CORRIDOR_LEN));
	corridorLeft[3].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(20.f, 0.f, right_z_pos += -CORRIDOR_LEN));

	wall[5].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(23.f, 0.f, right_z_pos - 1.f));

	this->wall_pos.push_back(glm::vec3(23.f, 0.f, right_z_pos - 1.f));
	this->wall_update_translate.push_back(0.f);

	iris[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(23.f, -3.f, right_z_pos- 1.f));
	iris_pos.push_back(glm::vec3(23.f, -3.f, right_z_pos - 1.f));
	iris_update_translate.push_back(0.f);

	corridorLeft[4].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(15.f, 0.f, right_z_pos += -CORRIDOR_LEN));

	wall[6].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(15.f, -3.f, right_z_pos-1.f)) * glm::translate(glm::mat4(1.f), glm::vec3(0.f, -0.5, 5.2)) * glm::rotate(glm::mat4(1.f), glm::radians(19.f), glm::vec3(0.f, 1.f, 0.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));

	this->wall_pos.push_back(glm::vec3(15.f, -3.f, right_z_pos - 1.f));
	this->wall_update_translate.push_back(0.f);

	cannonMount[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(17.f, -3.f, right_z_pos-2.f)) * glm::translate(glm::mat4(1.f), glm::vec3(0.8, -0.2, 5.f)) * glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(0.f, 1.f, 0.f)) * glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(1.f, 0.f, 0.f));
	cannon[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(17.f, -3.f, right_z_pos-2.f)) * glm::translate(glm::mat4(1.f), glm::vec3(0.f, 1.15, 5.f)) ;

	cannon_pos.push_back(glm::vec3(17.f, -3.f + 1.5, right_z_pos - 2.f + 5));
	cannon_update_radians.push_back(glm::vec3(0.f,0.f,0.f));

	corridorLeft[5].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(10.f, 0.f, right_z_pos += -CORRIDOR_LEN));

	//concatenate

	corridorFork[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -20.f-200.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-180.f), glm::vec3(1.f, 0.f, 0.f));

	corridorStraight[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -220.f));
	corridorStraight[3].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -240.f));

	wall[7].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(4.f, 0.f, -240.f + 1.f));

	this->wall_pos.push_back(glm::vec3(4.f, 0.f, -240.f + 1.f));
	this->wall_update_translate.push_back(0.f);

	iris[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(4.f, -3.f, -240.f + 1.f));
	iris_pos.push_back(glm::vec3(4.f, -3.f, -240.f + 1.f));
	iris_update_translate.push_back(0.f);

	corridorStraight[4].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -260.f));
	corridorStraight[5].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -280.f));
	corridorFork[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -300.f));

	corridorRight[6].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(5.f, 0.f, -320.f));
	wall[8].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(8.f, 0.f, -320.f - 1.f));

	corridorLeft[6].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 0.f, -320.f));

	//use this in order to hide light
	corridorStraight[6].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 2.f, -350.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));
	corridorStraight[7].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-15.f, 2.f, -350.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));
	corridorRight[7].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, -2.f, -350.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));
	corridorLeft[7].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-15.f, -2.f, -350.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));
	corridorStraight[8].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-10.f, 0.f, -5.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));

	pipe[5].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.2, 4.f, -262.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));
	pipe[6].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -260.f));
	pipe[7].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -270.f));
	pipe[8].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -280.f));
	pipe[9].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -290.f));
	pipe[10].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -300.f));
	pipe[11].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.2, 4.f, -307.5)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));


	CollidableNode& ch_beam = *this->m_collidables_nodes[0];

	std::vector<std::reference_wrapper <CollidableNode>> ch_pipe;
	for (int i = 1; i < 13; i++) {
		ch_pipe.push_back(*this->m_collidables_nodes[i]);
	}

	std::vector<std::reference_wrapper <CollidableNode>> ch_iris;
	for (int i = 11; i < 16; i++) {
		ch_iris.push_back(*this->m_collidables_nodes[i]);
	}

	std::vector<std::reference_wrapper <CollidableNode>> ch_cannonMount;
	for (int i = 16; i < 19; i++) {
		ch_cannonMount.push_back(*this->m_collidables_nodes[i]);
	}

	std::vector<std::reference_wrapper <CollidableNode>> ch_cannon;
	for (int i = 19; i < 22; i++) {
		ch_cannon.push_back(*this->m_collidables_nodes[i]);
	}

	std::vector<std::reference_wrapper <CollidableNode>> ch_wall;
	for (int i = 22; i < 31; i++) {
		ch_wall.push_back(*this->m_collidables_nodes[i]);
	}

	std::vector<std::reference_wrapper <CollidableNode>> ch_corridorFork;
	for (int i = 31; i < 34; i++) {
		ch_corridorFork.push_back(*this->m_collidables_nodes[i]);
	}

	std::vector<std::reference_wrapper <CollidableNode>> ch_corridorRight;
	for (int i = 34; i < 42; i++) {
		ch_corridorRight.push_back(*this->m_collidables_nodes[i]);
	}

	std::vector<std::reference_wrapper <CollidableNode>> ch_corridorLeft;
	for (int i = 42; i < 50; i++) {
		ch_corridorLeft.push_back(*this->m_collidables_nodes[i]);
	}

	std::vector<std::reference_wrapper <CollidableNode>> ch_corridorStraight;
	for (int i = 50; i < 60; i++) {
		ch_corridorStraight.push_back(*this->m_collidables_nodes[i]);
	}


	ch_corridorStraight[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -20.f));
	ch_corridorStraight[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -40.f));

	ch_pipe[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -20.f));
	ch_pipe[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -30.f));
	ch_pipe[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -40.f));
	ch_pipe[3].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -50.f));
	ch_pipe[4].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.2, 4.f, -57.5)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));



	//split
	ch_corridorFork[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -60.f));

	//left path
	left_z_pos = -60.f;
	ch_corridorLeft[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-5.f, 0.f, left_z_pos += -CORRIDOR_LEN));
	ch_corridorLeft[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-10.f, 0.f, left_z_pos += -CORRIDOR_LEN));

	ch_wall[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-6.f, 0.f, left_z_pos + 1.f));

	ch_corridorLeft[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-15.f, 0.f, left_z_pos += -CORRIDOR_LEN));

	ch_beam.model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-16.f, -3.f, left_z_pos - 1.f));

	ch_corridorRight[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-20.f, 0.f, left_z_pos += -CORRIDOR_LEN));
	ch_corridorRight[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-15.f, 0.f, left_z_pos += -CORRIDOR_LEN));

	ch_wall[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-15.f, -3.f, left_z_pos - 2.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-21.f), glm::vec3(0.f, 1.f, 0.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));

	ch_cannonMount[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-13.f, -2.5, left_z_pos - 6.f)) * glm::translate(glm::mat4(1.f), glm::vec3(0.8, -0.2, 5.f)) * glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(0.f, 1.f, 0.f)) * glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(1.f, 0.f, 0.f));
	ch_cannon[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-13.f, -2.5, left_z_pos - 6.f)) * glm::translate(glm::mat4(1.f), glm::vec3(0.f, 1.15, 5.f));

	ch_corridorRight[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-10.f, 0.f, left_z_pos += -CORRIDOR_LEN));

	ch_wall[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-14.f, 0.f, left_z_pos - 0.f));

	ch_cannonMount[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-14.f, -3.2, left_z_pos - 0.f)) * glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, 0.f)) * glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(1.f, 0.f, 0.f));
	ch_cannon[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-14.f, -3.f, left_z_pos - 0.f)) * glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.5, 1.f)) * glm::rotate(glm::mat4(1.f), glm::radians(90.f), glm::vec3(0.f, 0.f, 1.f));

	ch_iris[0].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(-14.f, 3.f, left_z_pos - 0.f));

	//right path
	right_z_pos = -60.f;
	ch_corridorRight[3].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(5.f, 0.f, right_z_pos += -CORRIDOR_LEN));

	ch_wall[3].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(8.f, 0.f, right_z_pos - 1.f));

	ch_corridorRight[4].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(10.f, 0.f, right_z_pos += -CORRIDOR_LEN));

	ch_wall[4].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(7.f, 0.f, right_z_pos - 1.5));

	ch_corridorRight[5].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(15.f, 0.f, right_z_pos += -CORRIDOR_LEN));
	ch_corridorLeft[3].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(20.f, 0.f, right_z_pos += -CORRIDOR_LEN));

	ch_wall[5].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(23.f, 0.f, right_z_pos - 1.f));

	ch_iris[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(23.f, -3.f, right_z_pos - 1.f));

	ch_corridorLeft[4].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(15.f, 0.f, right_z_pos += -CORRIDOR_LEN));

	ch_wall[6].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(15.f, -3.f, right_z_pos - 1.f)) * glm::translate(glm::mat4(1.f), glm::vec3(0.f, -0.5, 5.2)) * glm::rotate(glm::mat4(1.f), glm::radians(19.f), glm::vec3(0.f, 1.f, 0.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 0.f, 1.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));
	ch_cannonMount[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(17.f, -3.f, right_z_pos - 2.f)) * glm::translate(glm::mat4(1.f), glm::vec3(0.8, -0.2, 5.f)) * glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(0.f, 1.f, 0.f)) * glm::rotate(glm::mat4(1.f), glm::radians(180.f), glm::vec3(1.f, 0.f, 0.f));
	ch_cannon[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(17.f, -3.f, right_z_pos - 2.f)) * glm::translate(glm::mat4(1.f), glm::vec3(0.f, 1.15, 5.f));

	ch_corridorLeft[5].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(10.f, 0.f, right_z_pos += -CORRIDOR_LEN));

	//concatenate

	ch_corridorFork[1].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -20.f - 200.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-180.f), glm::vec3(1.f, 0.f, 0.f));

	ch_corridorStraight[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -220.f));
	ch_corridorStraight[3].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -240.f));

	ch_wall[7].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(4.f, 0.f, -240.f + 1.f));

	ch_iris[2].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(4.f, -3.f, -240.f + 1.f));

	ch_corridorStraight[4].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -260.f));
	ch_corridorStraight[5].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -280.f));

	//use this in order to hide light
	//ch_corridorRight[6].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -300.f));
	//ch_corridorLeft[6].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -300.f));
	//ch_corridorRight[7].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -320.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));

	ch_pipe[5].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.2, 4.f, -262.f)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));
	ch_pipe[6].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -260.f));
	ch_pipe[7].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -270.f));
	ch_pipe[8].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -280.f));
	ch_pipe[9].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -290.f));
	ch_pipe[10].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.f, 4.f, -300.f));
	ch_pipe[11].get().model_matrix = glm::translate(glm::mat4(1.f), glm::vec3(1.2, 4.f, -307.5)) * glm::rotate(glm::mat4(1.f), glm::radians(-90.f), glm::vec3(0.f, 1.f, 0.f));


	this->m_world_matrix = glm::scale(glm::mat4(1.f), glm::vec3(0.3, 0.3, 0.3));
}

void Renderer::InitCamera()
{
	this->m_camera_position = glm::vec3(0, 0, -5);
	this->m_camera_target_position = glm::vec3(0, 0, -9.5);
	this->m_camera_up_vector = glm::vec3(0, 1, 0);

	this->m_view_matrix = glm::lookAt(
		this->m_camera_position,
		this->m_camera_target_position,
		m_camera_up_vector);

	this->m_projection_matrix = glm::perspective(
		glm::radians(45.f),
		this->m_screen_width / (float)this->m_screen_height,
		0.1f, 100.f);
}

bool Renderer::InitLights()
{

	this->m_light.SetColor(glm::vec3(35.f));
	this->m_light.SetPosition(glm::vec3(0, 3, 4.5));
	this->m_light.SetTarget(glm::vec3(0));
	this->m_light.SetConeSize(40, 50);
	this->m_light.CastShadow(false);

	return true;
}

bool Renderer::InitShaders()
{
	std::string vertex_shader_path = "Assets/Shaders/geometry pass.vert";
	std::string geometry_shader_path = "Assets/Shaders/geometry pass.geom";
	std::string fragment_shader_path = "Assets/Shaders/geometry pass.frag";

	m_geometry_program.LoadVertexShaderFromFile(vertex_shader_path.c_str());
	m_geometry_program.LoadGeometryShaderFromFile(geometry_shader_path.c_str());
	m_geometry_program.LoadFragmentShaderFromFile(fragment_shader_path.c_str());
	m_geometry_program.CreateProgram();

	vertex_shader_path = "Assets/Shaders/deferred pass.vert";
	fragment_shader_path = "Assets/Shaders/deferred pass.frag";

	m_deferred_program.LoadVertexShaderFromFile(vertex_shader_path.c_str());
	m_deferred_program.LoadFragmentShaderFromFile(fragment_shader_path.c_str());
	m_deferred_program.CreateProgram();

	vertex_shader_path = "Assets/Shaders/post_process.vert";
	fragment_shader_path = "Assets/Shaders/post_process.frag";

	m_post_program.LoadVertexShaderFromFile(vertex_shader_path.c_str());
	m_post_program.LoadFragmentShaderFromFile(fragment_shader_path.c_str());
	m_post_program.CreateProgram();

	vertex_shader_path = "Assets/Shaders/shadow_map_rendering.vert";
	fragment_shader_path = "Assets/Shaders/shadow_map_rendering.frag";

	m_spot_light_shadow_map_program.LoadVertexShaderFromFile(vertex_shader_path.c_str());
	m_spot_light_shadow_map_program.LoadFragmentShaderFromFile(fragment_shader_path.c_str());
	m_spot_light_shadow_map_program.CreateProgram();

	return true;
}

bool Renderer::InitIntermediateBuffers()
{
	glGenTextures(1, &m_fbo_depth_texture);
	glGenTextures(1, &m_fbo_pos_texture);
	glGenTextures(1, &m_fbo_normal_texture);
	glGenTextures(1, &m_fbo_albedo_texture);
	glGenTextures(1, &m_fbo_mask_texture);
	glGenTextures(1, &m_fbo_texture);

	glGenFramebuffers(1, &m_fbo);

	return ResizeBuffers(m_screen_width, m_screen_height);
}

bool Renderer::ResizeBuffers(int width, int height)
{
	m_screen_width = width;
	m_screen_height = height;

	// texture
	glBindTexture(GL_TEXTURE_2D, m_fbo_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, m_fbo_pos_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, m_fbo_normal_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, m_fbo_albedo_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, m_fbo_mask_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_screen_width, m_screen_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, m_fbo_depth_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, m_screen_width, m_screen_height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

	// framebuffer to link to everything together
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fbo_pos_texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_fbo_normal_texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, m_fbo_albedo_texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, m_fbo_mask_texture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_fbo_depth_texture, 0);

	GLenum status = Tools::CheckFramebufferStatus(m_fbo);
	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		return false;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return true;
}

bool Renderer::InitCommonItems()
{
	glGenVertexArrays(1, &m_vao_fbo);
	glBindVertexArray(m_vao_fbo);

	GLfloat fbo_vertices[] = {
		-1, -1,
		1, -1,
		-1, 1,
		1, 1, };

	glGenBuffers(1, &m_vbo_fbo_vertices);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo_fbo_vertices);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fbo_vertices), fbo_vertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
	return true;
}

bool Renderer::InitGeometricMeshes()
{
	std::array<const char*, OBJECS::SIZE_ALL> assets = {
		"Assets/Beam.obj", 
		"Assets/Pipe.obj", 
		"Assets/Iris.obj",
		"Assets/CannonMount.obj",
		"Assets/Cannon.obj", 
		"Assets/Wall.obj", 
		"Assets/Corridor_Fork.obj", 
		"Assets/Corridor_Right.obj", 
		"Assets/Corridor_Left.obj", 
		"Assets/Corridor_Straight.obj" };


	std::array<const char*, OBJECS::SIZE_ALL> assets_collidable = {
		"Assets/CH-Beam.obj",
		"Assets/CH-Pipe.obj",
		"Assets/CH-Iris.obj",
		"Assets/CH-CannonMount.obj",
		"Assets/CH-Cannon.obj",
		"Assets/CH-Wall.obj",
		"Assets/CH-Corridor_Fork.obj",
		"Assets/CH-Corridor_Right.obj",
		"Assets/CH-Corridor_Left.obj",
		"Assets/CH-Corridor_Straight.obj" };

	bool initialized = true;

	//load real asset and collidable for scene
	
	//index 0 (beam) 1 times
	SaveMultipleGeometryNodeInstances(0, 1, initialized, assets, assets_collidable);

	//index 1 (pipe) 12 times
	SaveMultipleGeometryNodeInstances(1, 12, initialized, assets, assets_collidable);

	//index 2 (iris) 3 times
	SaveMultipleGeometryNodeInstances(2, 3, initialized, assets, assets_collidable);

	//index 3 (cannon_mount) 3 times
	SaveMultipleGeometryNodeInstances(3, 3, initialized, assets, assets_collidable);

	//index 4 (cannon) 3 times
	SaveMultipleGeometryNodeInstances(4, 3, initialized, assets, assets_collidable);

	//index 5 (wall) multiple 9 times
	SaveMultipleGeometryNodeInstances(5, 9, initialized, assets, assets_collidable);

	//index 6 (corridor_fork) 3 times
	SaveMultipleGeometryNodeInstances(6, 3, initialized, assets, assets_collidable);

	//index 7 (corridor_right) 8 times
	SaveMultipleGeometryNodeInstances(7, 8, initialized, assets, assets_collidable);

	//index 8 (corridor_left) 8 times
	SaveMultipleGeometryNodeInstances(8, 8, initialized, assets, assets_collidable);

	//index 9 (corridor_straight) 10 times
	SaveMultipleGeometryNodeInstances(9, 10, initialized, assets, assets_collidable);

	return initialized;
}

void Renderer::Update(float dt)
{
	this->UpdateGeometry(dt);
	this->UpdateCamera(dt);
	this->UpdateLights();
	m_continous_time += dt;
}

void Renderer::UpdateGeometry(float dt)
{

	//hita must be > to CANNON_ROT_ACCURACY as not to tremble
	glm::vec3 const CANNON_ROT_ACCURACY = glm::vec3(0.05,0.05,0);
	float const RANGE = 6;
	glm::vec3  const hita = glm::vec3(0.01, 0.01, 0.01);


	if (dt < 3) {
		for (int i = 0; i < m_nodes.size(); i++) {
			GeometryNode& gn = *this->m_nodes[i];
			gn.app_model_matrix = gn.model_matrix;
			CollidableNode& cn = *this->m_collidables_nodes[i];
			cn.app_model_matrix = cn.model_matrix;
		}
	}

	float trans_can = 0;

	//wall open/close
	for (int i = 0; i < wall_pos.size(); i++) {

		//i=1 and i=6 are paraller with xx'
		if(i==6 || i==1) continue;
		GeometryNode& wall = *this->m_nodes[i+22];
		CollidableNode& ch_wall = *this->m_collidables_nodes[i+22];

		if (abs(m_camera_position.z - wall_pos[i].z * 0.3) < RANGE) {
			if (wall_update_translate[i] < 3.5) {
				wall_update_translate[i] += hita.x*2;
				if (i == 2) {
					cannon_pos[1].x += hita.x * 2;
				}
			}
		}
		else {
			if (wall_update_translate[i] > 0.01) {
				wall_update_translate[i] -= hita.x*2;
				if (i == 2) {
					cannon_pos[1].x -= hita.x * 2;
				}
			}
		}
		wall.app_model_matrix =
			wall.model_matrix *
			glm::translate(glm::mat4(1.f), glm::vec3(wall_update_translate[i], 0.f, 0.f));
		ch_wall.app_model_matrix =
			ch_wall.model_matrix *
			glm::translate(glm::mat4(1.f), glm::vec3(wall_update_translate[i], 0.f, 0.f));

		if (i == 2) {
			trans_can = wall_update_translate[i];
			iris_update_translate[0] = wall_update_translate[i];
		}
		else if (i == 5) {
			iris_update_translate[1] = wall_update_translate[i];
		}
		else if (i == 7) {
			iris_update_translate[2] = wall_update_translate[i];
		}
	}

	//iris movement 
	for (int i = 0; i < iris_pos.size(); i++) {
		GeometryNode& iris = *this->m_nodes[i + 13];
		CollidableNode& ch_iris = *this->m_collidables_nodes[i + 13];
		iris.app_model_matrix =
			iris.model_matrix *
			glm::translate(glm::mat4(1.f), glm::vec3(iris_update_translate[i], 0.f, 0.f));
		ch_iris.app_model_matrix =
			ch_iris.model_matrix *
			glm::translate(glm::mat4(1.f), glm::vec3(iris_update_translate[i], 0.f, 0.f));
	}

	//rotate cannon

	for (int i = 0; i < cannon_pos.size(); i++) {
		GeometryNode& cannonMount = *this->m_nodes[16+i];
		CollidableNode& ch_cannonMount = *this->m_collidables_nodes[16+i];
		GeometryNode& cannon = *this->m_nodes[19+i];
		CollidableNode& ch_cannon = *this->m_collidables_nodes[19+i];

		//cannon range
		if ((m_camera_position.z - cannon_pos[i].z * 0.3 > 0) &&
			(abs(m_camera_position.z - cannon_pos[i].z * 0.3) < RANGE + 1.5) &&
			(abs(m_camera_position.x - cannon_pos[i].x * 0.3) < RANGE + 1.5)) {

			//x axis
			float x_curr = tan(cannon_update_radians[i].x) * (m_camera_position.z - cannon_pos[i].z * 0.3);
			float x = cannon_pos[i].x * 0.3 - m_camera_position.x;

			if (abs(abs(x_curr) - abs(x)) < CANNON_ROT_ACCURACY.x) {
				//accuracy
			}
			else if (x_curr < x) {
				cannon_update_radians[i].x += hita.x;
			}
			else {
				cannon_update_radians[i].x -= hita.x;
			}

			//y axis
			float y_curr = tan(cannon_update_radians[i].y) * (m_camera_position.z - cannon_pos[i].z * 0.3);
			float y = cannon_pos[i].y * 0.3 - m_camera_position.y;

			if (abs(abs(y_curr) - abs(y)) < CANNON_ROT_ACCURACY.y) {
				//accuracy
			}
			else if (y_curr < y) {
				cannon_update_radians[i].y += hita.y;
			}
			else {
				cannon_update_radians[i].y -= hita.y;
			}

			if (i != 1) {
				cannonMount.app_model_matrix =
					cannonMount.model_matrix *
					glm::rotate(glm::mat4(1.f), cannon_update_radians[i].x, glm::vec3(0.f, 1.f, 0.f));
				ch_cannonMount.app_model_matrix =
					ch_cannonMount.model_matrix *
					glm::rotate(glm::mat4(1.f), cannon_update_radians[i].x, glm::vec3(0.f, 1.f, 0.f));
				cannon.app_model_matrix =
					cannon.model_matrix *
					glm::rotate(glm::mat4(1.f), -cannon_update_radians[i].x, glm::vec3(0.f, 1.f, 0.f)) *
					glm::rotate(glm::mat4(1.f), cannon_update_radians[i].y, glm::vec3(1.f, 0.f, 0.f));
				ch_cannon.app_model_matrix =
					ch_cannon.model_matrix *
					glm::rotate(glm::mat4(1.f), -cannon_update_radians[i].x, glm::vec3(0.f, 1.f, 0.f)) *
					glm::rotate(glm::mat4(1.f), cannon_update_radians[i].y, glm::vec3(1.f, 0.f, 0.f));
			}

		}

		//we dont care about range here it has to run every time
		if (i == 1) {
			cannonMount.app_model_matrix =
				cannonMount.model_matrix *
				glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -trans_can));
			ch_cannonMount.app_model_matrix =
				ch_cannonMount.model_matrix *
				glm::translate(glm::mat4(1.f), glm::vec3(0.f, 0.f, -trans_can));
			cannon.app_model_matrix =
				cannon.model_matrix *
				glm::translate(glm::mat4(1.f), glm::vec3(0.f, -trans_can, 0.f)) *
				glm::rotate(glm::mat4(1.f), -cannon_update_radians[i].x, glm::vec3(1.f, 0.f, 0.f));
			ch_cannon.app_model_matrix =
				ch_cannon.model_matrix *
				glm::translate(glm::mat4(1.f), glm::vec3(0.f, -trans_can, 0.f)) *
				glm::rotate(glm::mat4(1.f), -cannon_update_radians[i].x, glm::vec3(1.f, 0.f, 0.f));
		}
	}

}

void Renderer::UpdateCamera(float dt)
{
	glm::vec3 direction = glm::normalize(m_camera_target_position - m_camera_position);

	const int DIR_SIZE = 8;

	glm::vec3 directions[DIR_SIZE];

	int index = 0;
	for (int theta = 0; theta < 360; theta += 360/DIR_SIZE) {
		directions[index] = glm::vec3(sin(theta) * direction.x, direction.y, cos(theta) * direction.z);
		index++;
	}

	glm::vec3 right = glm::normalize(glm::cross(direction, m_camera_up_vector));

	float_t isectF = 0.f;
	float_t isectMin = 0.f;
	float_t isectMax = 4.f;
	for (auto& node : this->m_collidables_nodes)
	{
		for (int i = 0; i < DIR_SIZE; i++) {
			if (node->intersectRay((m_camera_movement.y * 5.f * dt) * right + m_camera_position + (m_camera_movement.x * 5.f * dt) * direction, directions[i], m_world_matrix, isectF, isectMax, isectMin)) {
				printf("Debug. Collision theta = %d %f",i*360/DIR_SIZE, isectF);
				printf("\n");
				return;
			}
		}
	}

	m_camera_position = m_camera_position + (m_camera_movement.x * 5.f * dt) * direction;
	m_camera_target_position = m_camera_target_position + (m_camera_movement.x * 5.f * dt) * direction;


	m_camera_position = m_camera_position + (m_camera_movement.y * 5.f * dt) * right;
	m_camera_target_position = m_camera_target_position + (m_camera_movement.y * 5.f * dt) * right;

	float speed = glm::pi<float>() * 0.002;
	glm::mat4 rotation = glm::rotate(glm::mat4(1.f), m_camera_look_angle_destination.y * speed, right);
	rotation *= glm::rotate(glm::mat4(1.f), m_camera_look_angle_destination.x * speed, m_camera_up_vector);
	m_camera_look_angle_destination = glm::vec2(0.f);

	direction = rotation * glm::vec4(direction, 0.f);


	m_camera_target_position = m_camera_position + direction * glm::distance(m_camera_position, m_camera_target_position);

	m_view_matrix = glm::lookAt(m_camera_position, m_camera_target_position, m_camera_up_vector);

	//infinite path - world scaled, so does camera (0.3)
	if (m_camera_position.z < -(268*0.3)) {
		m_camera_position.z = -18.9*0.3;
	}

	//std::cout << m_camera_position.x << " " << m_camera_position.y << " " << m_camera_position.z << " " << std::endl;
	//std::cout << m_camera_target_position.x << " " << m_camera_target_position.y << " " << m_camera_target_position.z << " " << std::endl;
	//m_light.SetPosition(m_camera_position);
	//m_light.SetTarget(m_camera_target_position);
}

bool Renderer::UpdateLights()
{
	m_light.SetPosition(m_camera_position);
	m_light.SetTarget(m_camera_target_position);

	return true;
}

bool Renderer::ReloadShaders()
{
	m_geometry_program.ReloadProgram();
	m_post_program.ReloadProgram();
	m_deferred_program.ReloadProgram();
	m_spot_light_shadow_map_program.ReloadProgram();
	return true;
}

void Renderer::Render()
{
	RenderShadowMaps();
	RenderGeometry();
	RenderDeferredShading();
	RenderPostProcess();

	GLenum error = Tools::CheckGLError();

	if (error != GL_NO_ERROR)
	{
		printf("Reanderer:Draw GL Error\n");
		system("pause");
	}
}

void Renderer::RenderPostProcess()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClearColor(0.f, 0.8f, 1.f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);

	m_post_program.Bind();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_fbo_texture);
	m_post_program.loadInt("uniform_texture", 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_light.GetShadowMapDepthTexture());
	m_post_program.loadInt("uniform_shadow_map", 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_fbo_pos_texture);
	m_post_program.loadInt("uniform_tex_pos", 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_fbo_normal_texture);
	m_post_program.loadInt("uniform_tex_normal", 3);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_fbo_albedo_texture);
	m_post_program.loadInt("uniform_tex_albedo", 4);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, m_fbo_mask_texture);
	m_post_program.loadInt("uniform_tex_mask", 5);

	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, m_fbo_depth_texture);
	m_post_program.loadInt("uniform_tex_depth", 6);

	glBindVertexArray(m_vao_fbo);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
	m_post_program.Unbind();
}

void Renderer::SaveMultipleGeometryNodeInstances(int assets_index, int max, bool& initialized, std::array<const char*, SIZE_ALL> assets, std::array<const char*, SIZE_ALL> assets_collidable)
{
	OBJLoader loader;
	GeometricMesh* mesh = loader.load(assets[assets_index]);

	for (int i = 0; i < max; i++) {

		if (mesh != nullptr)
		{
			GeometryNode* node = new GeometryNode();
			node->Init(mesh);
			this->m_nodes.push_back(node);
			if (i == max-1) {
				delete mesh;
			}
		}
		else
		{
			initialized = false;
		}
	}

	GeometricMesh* mesh2 = loader.load(assets_collidable[assets_index]);

	for (int i = 0; i < max; i++) {

		if (mesh2 != nullptr)
		{
			CollidableNode* coll_node = new CollidableNode();
			coll_node->Init(mesh2);
			this->m_collidables_nodes.push_back(coll_node);
			if (i == max - 1) {
				delete mesh2;
			}
		}
		else
		{
			initialized = false;
		}
	}

}

void Renderer::RenderStaticGeometry()
{
	glm::mat4 proj = m_projection_matrix * m_view_matrix * m_world_matrix;

	for (auto& node : this->m_nodes)
	{
		glBindVertexArray(node->m_vao);

		m_geometry_program.loadMat4("uniform_projection_matrix", proj * node->app_model_matrix);
		m_geometry_program.loadMat4("uniform_normal_matrix", glm::transpose(glm::inverse(m_world_matrix * node->app_model_matrix)));
		m_geometry_program.loadMat4("uniform_world_matrix", m_world_matrix * node->app_model_matrix);

		for (int j = 0; j < node->parts.size(); ++j)
		{
			m_geometry_program.loadVec3("uniform_diffuse", node->parts[j].diffuse);
			m_geometry_program.loadVec3("uniform_ambient", node->parts[j].ambient);
			m_geometry_program.loadVec3("uniform_specular", node->parts[j].specular);
			m_geometry_program.loadFloat("uniform_shininess", node->parts[j].shininess);
			m_geometry_program.loadInt("uniform_has_tex_diffuse", (node->parts[j].diffuse_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_emissive", (node->parts[j].emissive_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_mask", (node->parts[j].mask_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_normal", (node->parts[j].bump_textureID > 0 || node->parts[j].normal_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_is_tex_bumb", (node->parts[j].bump_textureID > 0) ? 1 : 0);

			glActiveTexture(GL_TEXTURE0);
			m_geometry_program.loadInt("uniform_tex_diffuse", 0);
			glBindTexture(GL_TEXTURE_2D, node->parts[j].diffuse_textureID);

			if (node->parts[j].mask_textureID > 0)
			{
				glActiveTexture(GL_TEXTURE1);
				m_geometry_program.loadInt("uniform_tex_mask", 1);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].mask_textureID);
			}

			if ((node->parts[j].bump_textureID > 0 || node->parts[j].normal_textureID > 0))
			{
				glActiveTexture(GL_TEXTURE2);
				m_geometry_program.loadInt("uniform_tex_normal", 2);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].bump_textureID > 0 ?
					node->parts[j].bump_textureID : node->parts[j].normal_textureID);
			}

			if (node->parts[j].emissive_textureID > 0)
			{
				glActiveTexture(GL_TEXTURE3);
				m_geometry_program.loadInt("uniform_tex_emissive", 3);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].emissive_textureID);
			}

			glDrawArrays(GL_TRIANGLES, node->parts[j].start_offset, node->parts[j].count);
		}

		glBindVertexArray(0);
	}
}

void Renderer::RenderCollidableGeometry()
{
	glm::mat4 proj = m_projection_matrix * m_view_matrix * m_world_matrix;

	glm::vec3 camera_dir = normalize(m_camera_target_position - m_camera_position);
	float_t isectT = 0.f;

	for (auto& node : this->m_collidables_nodes)
	{
		int32_t primID = -1;
		int32_t totalRenderedPrims = 0;
		if (node->intersectRay(m_camera_position, camera_dir, m_world_matrix, isectT)) continue;

		glBindVertexArray(node->m_vao);

		m_geometry_program.loadMat4("uniform_projection_matrix", proj * node->app_model_matrix);
		m_geometry_program.loadMat4("uniform_normal_matrix", glm::transpose(glm::inverse(m_world_matrix * node->app_model_matrix)));
		m_geometry_program.loadMat4("uniform_world_matrix", m_world_matrix * node->app_model_matrix);
		m_geometry_program.loadFloat("uniform_time", m_continous_time);

		for (int j = 0; j < node->parts.size(); ++j)
		{
			m_geometry_program.loadVec3("uniform_diffuse", node->parts[j].diffuse);
			m_geometry_program.loadVec3("uniform_ambient", node->parts[j].ambient);
			m_geometry_program.loadVec3("uniform_specular", node->parts[j].specular);
			m_geometry_program.loadFloat("uniform_shininess", node->parts[j].shininess);
			m_geometry_program.loadInt("uniform_has_tex_diffuse", (node->parts[j].diffuse_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_mask", (node->parts[j].mask_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_emissive", (node->parts[j].emissive_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_has_tex_normal", (node->parts[j].bump_textureID > 0 || node->parts[j].normal_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_is_tex_bumb", (node->parts[j].bump_textureID > 0) ? 1 : 0);
			m_geometry_program.loadInt("uniform_prim_id", primID - totalRenderedPrims);

			glActiveTexture(GL_TEXTURE0);
			m_geometry_program.loadInt("uniform_tex_diffuse", 0);
			glBindTexture(GL_TEXTURE_2D, node->parts[j].diffuse_textureID);

			if (node->parts[j].mask_textureID > 0)
			{
				glActiveTexture(GL_TEXTURE1);
				m_geometry_program.loadInt("uniform_tex_mask", 1);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].mask_textureID);
			}

			if ((node->parts[j].bump_textureID > 0 || node->parts[j].normal_textureID > 0))
			{
				glActiveTexture(GL_TEXTURE2);
				m_geometry_program.loadInt("uniform_tex_normal", 2);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].bump_textureID > 0 ?
					node->parts[j].bump_textureID : node->parts[j].normal_textureID);
			}

			if (node->parts[j].emissive_textureID > 0)
			{
				glActiveTexture(GL_TEXTURE3);
				m_geometry_program.loadInt("uniform_tex_emissive", 3);
				glBindTexture(GL_TEXTURE_2D, node->parts[j].emissive_textureID);
			}

			glDrawArrays(GL_TRIANGLES, node->parts[j].start_offset, node->parts[j].count);
			totalRenderedPrims += node->parts[j].count;
		}

		glBindVertexArray(0);
	}
}

void Renderer::RenderDeferredShading()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fbo_texture, 0);

	GLenum drawbuffers[1] = { GL_COLOR_ATTACHMENT0 };

	glDrawBuffers(1, drawbuffers);

	glViewport(0, 0, m_screen_width, m_screen_height);

	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);

	glClear(GL_COLOR_BUFFER_BIT);

	m_deferred_program.Bind();

	m_deferred_program.loadVec3("uniform_light_color", m_light.GetColor());
	m_deferred_program.loadVec3("uniform_light_dir", m_light.GetDirection());
	m_deferred_program.loadVec3("uniform_light_pos", m_light.GetPosition());

	m_deferred_program.loadFloat("uniform_light_umbra", m_light.GetUmbra());
	m_deferred_program.loadFloat("uniform_light_penumbra", m_light.GetPenumbra());

	m_deferred_program.loadVec3("uniform_camera_pos", m_camera_position);
	m_deferred_program.loadVec3("uniform_camera_dir", normalize(m_camera_target_position - m_camera_position));

	m_deferred_program.loadMat4("uniform_light_projection_view", m_light.GetProjectionMatrix() * m_light.GetViewMatrix());
	m_deferred_program.loadInt("uniform_cast_shadows", m_light.GetCastShadowsStatus() ? 1 : 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_fbo_pos_texture);
	m_deferred_program.loadInt("uniform_tex_pos", 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_fbo_normal_texture);
	m_deferred_program.loadInt("uniform_tex_normal", 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_fbo_albedo_texture);
	m_deferred_program.loadInt("uniform_tex_albedo", 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_fbo_mask_texture);
	m_deferred_program.loadInt("uniform_tex_mask", 3);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_fbo_depth_texture);
	m_deferred_program.loadInt("uniform_tex_depth", 4);

	glActiveTexture(GL_TEXTURE10);
	glBindTexture(GL_TEXTURE_2D, m_light.GetShadowMapDepthTexture());
	m_deferred_program.loadInt("uniform_shadow_map", 10);

	glBindVertexArray(m_vao_fbo);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);

	m_deferred_program.Unbind();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDepthMask(GL_TRUE);
}

void Renderer::RenderGeometry()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_fbo_pos_texture, 0);

	GLenum drawbuffers[4] = {
		GL_COLOR_ATTACHMENT0,
		GL_COLOR_ATTACHMENT1,
		GL_COLOR_ATTACHMENT2,
		GL_COLOR_ATTACHMENT3 };

	glDrawBuffers(4, drawbuffers);

	glViewport(0, 0, m_screen_width, m_screen_height);
	glClearColor(0.f, 0.1f, 0.1f, 0.1f);
	glClearDepth(1.f);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);
	glEnable(GL_DEPTH_TEST);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_geometry_program.Bind();
	RenderStaticGeometry();
	//RenderCollidableGeometry();

	m_geometry_program.Unbind();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	
}

void Renderer::RenderShadowMaps()
{
	if (m_light.GetCastShadowsStatus())
	{
		int m_depth_texture_resolution = m_light.GetShadowMapResolution();

		glBindFramebuffer(GL_FRAMEBUFFER, m_light.GetShadowMapFBO());
		glViewport(0, 0, m_depth_texture_resolution, m_depth_texture_resolution);
		glEnable(GL_DEPTH_TEST);
		glClear(GL_DEPTH_BUFFER_BIT);

		// Bind the shadow mapping program
		m_spot_light_shadow_map_program.Bind(); // !!!!

		glm::mat4 proj = m_light.GetProjectionMatrix() * m_light.GetViewMatrix() * m_world_matrix;

		for (auto& node : this->m_nodes)
		{
			glBindVertexArray(node->m_vao);

			m_spot_light_shadow_map_program.loadMat4("uniform_projection_matrix", proj * node->app_model_matrix);

			for (int j = 0; j < node->parts.size(); ++j)
			{
				glDrawArrays(GL_TRIANGLES, node->parts[j].start_offset, node->parts[j].count);
			}

			glBindVertexArray(0);
		}

		//glm::vec3 camera_dir = normalize(m_camera_target_position - m_camera_position);
		//float_t isectT = 0.f;

		//for (auto& node : this->m_collidables_nodes)
		//{
		//	if (node->intersectRay(m_camera_position, camera_dir, m_world_matrix, isectT)) continue;

		//	glBindVertexArray(node->m_vao);

		//	m_spot_light_shadow_map_program.loadMat4("uniform_projection_matrix", proj * node->app_model_matrix);

		//	for (int j = 0; j < node->parts.size(); ++j)
		//	{
		//		glDrawArrays(GL_TRIANGLES, node->parts[j].start_offset, node->parts[j].count);
		//	}

		//	glBindVertexArray(0);
		//}

		m_spot_light_shadow_map_program.Unbind();
		glDisable(GL_DEPTH_TEST);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
}

void Renderer::CameraMoveForward(bool enable)
{
	m_camera_movement.x = (enable) ? 1 : 0;
}
void Renderer::CameraMoveBackWard(bool enable)
{
	m_camera_movement.x = (enable) ? -1 : 0;
}

void Renderer::CameraMoveLeft(bool enable)
{
	m_camera_movement.y = (enable) ? -1 : 0;
}
void Renderer::CameraMoveRight(bool enable)
{
	m_camera_movement.y = (enable) ? 1 : 0;
}

void Renderer::CameraLook(glm::vec2 lookDir)
{
	m_camera_look_angle_destination = lookDir;
}