# AUEB graphics project

Computer game using OpenGL graphics engine.  
Dataset taken from [here](https://auebgr-my.sharepoint.com/:u:/g/personal/gepap_aueb_gr/EUxr0uEGsn1IosjQw8l8hDsBrxbK7DFXF9lWBBHy4F0iNg?e=ow5hVi).  
Teaser video in [here](https://auebgr-my.sharepoint.com/:v:/g/personal/gepap_aueb_gr/EajkX8vcCTBAhFB64HUu5CoBpSzlTlfKTwvipsUGFXa3fA?e=LeQiiA).
